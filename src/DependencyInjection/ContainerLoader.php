<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpFoundation\Response;

class ContainerLoader {

    private string $path;
    private string $filename;
    private string $project_dir;

    public function __construct(string $path, string $filename, string $project_dir)
    {
        $this->path = $path;
        $this->filename = $filename;
        $this->project_dir = $project_dir;
    }

    public function configure(): ContainerBuilder {

        $containerBuilder = new ContainerBuilder();
        $loader = new YamlFileLoader($containerBuilder, new FileLocator($this->path));
        $loader->load($this->filename);
        $containerBuilder->setParameter('project_dir', $this->project_dir);
        return $containerBuilder;
//
//        $containerBuilder->register('response', Response::class);
//        $containerBuilder->register('app', App::class)->addArgument(new Reference('response'));

    }
}