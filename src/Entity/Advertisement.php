<?php

namespace Aleksandr\KomitetTest\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="advertisement")
 */
class Advertisement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private string $text;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private int $price;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private int $limits;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private string $banner;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getLimits(): int
    {
        return $this->limits;
    }

    /**
     * @param int $limits
     */
    public function setLimits(int $limits): void
    {
        $this->limits = $limits;
    }

    /**
     * @return string
     */
    public function getBanner(): string
    {
        return $this->banner;
    }

    /**
     * @param string $banner
     */
    public function setBanner(string $banner): void
    {
        $this->banner = $banner;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}