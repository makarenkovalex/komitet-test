<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\Service;

use Aleksandr\KomitetTest\Entity\Advertisement;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository\RepositoryFactory;
use Symfony\Component\HttpFoundation\Request;

class AdCreator {

    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(Request $request): Advertisement
    {
        $ad = new Advertisement();
        $ad->setText($request->get('text'));
        $ad->setPrice((int)$request->get('price'));
        $ad->setLimits((int)$request->get('limit'));
        $ad->setBanner($request->get('banner'));
        $this->entityManager->persist($ad);
        $this->entityManager->flush();
        return $ad;
    }
}