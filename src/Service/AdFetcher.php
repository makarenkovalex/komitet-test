<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\Service;

use Aleksandr\KomitetTest\Entity\Advertisement;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Request;
use function Doctrine\ORM\QueryBuilder;
use function PHPUnit\Framework\isEmpty;

class AdFetcher {
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function get(Request $request): Advertisement
    {

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('a, max(a.price)')
            ->from(Advertisement::class, 'a')
            ;

        $result = $qb->getQuery()->getSingleResult();

        if (is_array($result) && isset($result[0])) {
            // get an object
            $result = $result[0];
            if ($result !== null) {
                return $result;
            }
        }

        throw new \Exception("no relevant advertisement");
    }
}